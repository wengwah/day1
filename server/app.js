console.log("Hello World2");
var x = 7
y = x + 1
console.log(y);

// this is a code comment line

/*
  This is a code comment block.

*/

var name = "Kenneth Phang";
console.log(typeof(name));

var age = 25.76868678;
console.log(typeof(age));

var smart = true;
console.log(typeof(smart));

var fruits = [
  "Orange",
  "Apple",
  "Durian",
  "Banana"
];
console.log(fruits);
console.log(typeof(fruits));
console.log("Fruit in position 0 is " + fruits[0])

var pos = fruits.indexOf("Banana");
console.log("Position of Banana is " + pos);

function printFruits(fruit_, index_1) {
  console.log("Index > " + index_1);
  console.log("Fruits > " + fruit_)
}

fruits.forEach(printFruits);

console.log("_____CALLBACK_______");

fruits.forEach(function(element, index_1) {
  console.log("Index > " + element);
  console.log("Fruits > " + index_1);
});

console.log("_____ORIGINAL ARRAY_______");

console.log(fruits);

console.log("_____PUSH RAMBUTAN_______");

fruits.push("Rambutan");
console.log(fruits);

console.log("_____POP LAST ITEM_______");

fruits.pop();
console.log(fruits);

console.log("_____SHIFT (REMOVE) FIRST ITEM_______");

fruits.shift();
console.log(fruits);

console.log("_____UNSHIFT (ADD TO BEGINNING OF ARRAY)_______");

fruits.unshift("Mango", "Tangerine");
console.log(fruits);

console.log("_____SPLICE_______");

fruits.splice(1, 4);
console.log(fruits);

console.log("_____SORT METHOD (ASCENDING ORDER)_______");

var numbers = [4, 2, 5, 1, 3];
numbers.sort();
console.log(numbers);

console.log("_____SORT METHOD MODIFIED (DESCENDING ORDER)_______");

numbers.sort(function(a,b) {
  return b - a;
});
console.log(numbers);

console.log("_____ALGORITHM FOR SORT METHOD_______");

var numbers = [4, 2, 5, 1, 3];
console.log(numbers);
numbers.sort(function(a,b) {
  console.log("a is " + a);
  console.log("b is " + b);
  return b - a;
});
console.log(numbers);


console.log("________Key-Value Pairs________")

var member = {
  name: "Ron",
  age: 18,
  gender: "M"
};

console.log(member);
console.log(typeof(member));
console.log("What is the name of the member? " + member.name)

// Conditionals

if (x > 10) {
  console.log("More than 10");
} else if (x <= 7 && x >= 5) {
  console.log("In between 5 and 7");
} else {
  console.log("Otherwise");
}

var x = 2;
switch (x) {
  case 1:
    console.log("This is 1");
    break;
  case 2:
    console.log("This is 2");
}

// for (var i = 0, i < fruits.length, i++) {
//   console.log(i + "fruit is " + fruits[i]);
// }

function hi(name) {
  console.log("Hello ! -- " + name);
}

function sayHi(callback) {
  console.log("Inside sayHi");
  var name = "Kenneth";
  callback(name);
}

sayHi(hi);

var i = 5;
while (i < 10) {
  console.log(i);
  i++;
}

var obj = {
  name: "Ron Chan",
  age: 18,
  myJob: "Software Engineer",
  "my vehicle": "Audi R8",
  hello: function() {
    console.log("Say Hi" + name);
  }
};

console.log(obj.name, obj.age, obj.myJob); // Case dependent
console.log(obj['name'], obj['age'], obj['my vehicle']);

var author = {
  name: "Ron Chan"
};

var publisher = {
  name: "SPH"
};

var book = {
  name: "A guide to the universe",
  author: author,
  publisher: publisher,
  noOfCopies: 100
};

console.log(book.author, book.publisher);
